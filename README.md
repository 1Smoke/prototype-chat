* [ ]  Dentro do aplicativo de bate-papo, será necessário abrir 2 telas: 
Na primeira tela, escolher um nome e entrar no chat ; 
Na segunda tela, a sala de bate-papo com mensagens .


*  Versão do socket server utilizada: " 1.0.0"

*  Versão do socket.io utilizada: "2.0.3"

*  Versão do express utilizada: "4.15.3"

*  main: "index.js"

*  Criando um novo Servidor de nós:

1.  mkdir SocketServer && cd SocketServer
2.  npm init
3.  npm install --save express socket.io

*  Para acessar: 

  
* [ ]  **http : // localhost : 3001**

*  Para Iniciar o Servidor:  

  
* [ ]  **node index.js**

*  Iniciando o aplicativo Prototype Chat

1.  ionic start Prototype-Chat blank
2.  cd Prototype-Chat
3.  npm install ng-socket-io --save
4.  ionic g page chatRoom